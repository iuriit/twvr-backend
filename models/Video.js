var mongoose = require('mongoose');
var SchemaTypes = mongoose.Schema.Types;

var VideoSchema = new mongoose.Schema({
    id: SchemaTypes.Number,
    title: String,
    description: String,
    thumbnail: String,
    video: String,
    category: String,
    tag: String,
    owner: String,
}, {timestamps: true});

VideoSchema.methods.toJSON = function(){
    return {
        _id: this._id,
        id: this.id,
        title: this.title,
        description: this.description,
        thumbnail: this.thumbnail,
        video: this.video,
        category: this.category,
        tag: this.tag,
        owner: this.owner,
    };
};

mongoose.model('Video', VideoSchema, 'video');