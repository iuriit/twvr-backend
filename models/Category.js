var mongoose = require('mongoose');
var SchemaTypes = mongoose.Schema.Types;

var CategorySchema = new mongoose.Schema({
    id: SchemaTypes.Number,
    name: {type: String, unique: true},
    owner: String,
}, {timestamps: true});

CategorySchema.methods.toJSON = function(){
    return {
        _id: this._id,
        id: this.id,
        name: this.name,
        owner: this.owner,
    };
};

mongoose.model('Category', CategorySchema, 'category');