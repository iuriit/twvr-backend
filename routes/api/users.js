var mongoose = require('mongoose');
var router = require('express').Router();
var passport = require('passport');
var User = mongoose.model('User');
var auth = require('../auth');

router.get('/', auth.required, function(req, res, next){
  User.findById(req.payload.id).then(function(user){
    if(!user) {
      return res.status(404).json({errors: {'user': 'does not exist'}});
    }
    if(user.role !== 'admin') {
      return res.status(403).json({errors: {'user': 'is not admin'}});
    }

    var countQuery = {role: 'user'};
    User.count(countQuery).then(function(count) {
      var sortQuery = {};
      sortQuery[req.query._sort] = (req.query._order === 'DESC') ? -1 : 1;
      User.find(countQuery).skip(parseInt(req.query._start)).limit(req.query._end - req.query._start).sort(sortQuery).then(function(items){
        return res.status(200).header({
                  'X-Total-Count': count,
                  'Access-Control-Expose-Headers': 'X-Total-Count',
                }).json(items.map(function(item, idx){
                  return item.toJSON();
                }));
      }).catch(next);
    }).catch(next);
  })
});

router.get('/:id', auth.required, function(req, res, next){
  User.findById(req.payload.id).then(function(user){
    if(!user ) {
      return res.status(404).json({errors: {'user': 'does not exist'}});
    }
    if(user.role !== 'admin') {
      return res.status(403).json({errors: {'user': 'is not admin'}});
    }

    User.findOne({_id: req.params.id}).then(function(user1){
      if(!user1) {
        return res.status(404).json({errors: {'user': 'does not exist'}});
      } else {
        return user1.toJSON();
      }
    }).catch(next);
  });
});

router.post('/', auth.required, function(req, res, next){
  User.findById(req.payload.id).then(function(user){
    if(!user) {
      return res.status(404).json({errors: {'user': 'does not exist'}});
    }
    if(user.role !== 'admin') {
      return res.status(403).json({errors: {'user': 'is not admin'}});
    }

    User.count({username: req.body.username}).then(function(count){
      if(count > 0) {
        return res.status(404).json({errors: {'user': 'already exists'}});
      } else {
        var user1 = new User();
        user1.username = req.body.username;
        user1.setPassword(req.body.password);
        user1.role = 'user';
        user1.save().then(function(){
          return res.status(200).json(user1.toJSON());
        }).catch(next);
      }
    })
  }).catch(next);    
});

router.put('/:id', auth.required, function(req, res, next){
  User.findById(req.payload.id).then(function(user){
    if(!user) {
      return res.status(404).json({errors: {'user': 'does not exist'}});
    }
    if(user.role !== 'admin') {
      return res.status(403).json({errors: {'user': 'is not admin'}});
    }
      
    User.findOne({_id: req.params.id}).then(function(user1){
      if(!user1) {
        return res.status(404).json({errors: {'user': 'does not exist'}});
      } else {
        user1.setPassword(req.body.password);
        user1.save().then(function(){
          return res.status(200).json(user1.toJSON());
        }).catch(next);
      }
    }).catch(next);
  }).catch(next);    
});

router.delete('/:id', auth.required, function(req, res, next){
  User.findById(req.payload.id).then(function(user){
    if(!user) {
      return res.status(404).json({errors: {'user': 'does not exist'}});
    }
    if(user.role !== 'admin') {
      return res.status(403).json({errors: {'user': 'is not admin'}});
    }
      
    User.findOneAndRemove({_id: req.params.id}).then(function(){
      return res.status(200).json({success: 'success'});
    }).catch(next);
  }).catch(next);    
});

module.exports = router;
