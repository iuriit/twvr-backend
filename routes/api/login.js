var mongoose = require('mongoose');
var router = require('express').Router();
var passport = require('passport');
var User = mongoose.model('User');
var auth = require('../auth');

router.post('/login', function(req, res, next){
  if(!req.body.user.username){
    return res.status(401).json({errors: {username: "can't be blank"}});
  }

  if(!req.body.user.password){
    return res.status(401).json({errors: {password: "can't be blank"}});
  }

  passport.authenticate('local', {session: false}, function(err, user, info){
    if(err){ return next(err); }

    if(user){
      user.token = user.generateJWT();
      return res.status(200).json({user: user.toJSON()});
    } else {
      return res.status(401).json(info);
    }
  })(req, res, next);
});

router.post('/register', function(req, res, next){
  var user = new User();
  user.username = req.body.user.username;
  user.setPassword(req.body.user.password);
  // user.email = req.body.email;
  // user.name = req.body.name;
  user.role = req.body.user.role;
  user.save().then(function(){
    return res.status(200).json({user: user.toJSON()});
  }).catch(next);
});

// router.get('/user', auth.required, function(req, res, next){
//   User.findById(req.payload.id).then(function(user){
//     if(!user){ return res.sendStatus(401).json({errors: {user: 'does not exist'}}); }

//     return res.json({user: user.toJSON()});
//   }).catch(next);
// });

module.exports = router;
