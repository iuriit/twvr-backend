var router = require('express').Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Category = mongoose.model('Category');
var Video = mongoose.model('Video');
var auth = require('../auth');
var AWS = require('aws-sdk');
var multer = require('multer');
var multerS3 = require('multer-s3');

const BUCKET_NAME = 'takeawalk';
const IAM_USER_KEY = 'AKIAI22D54BHRJ7ZZUNQ';
const IAM_USER_SECRET = '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';

const s3 = new AWS.S3({
    accessKeyId: IAM_USER_KEY,
    secretAccessKey: IAM_USER_SECRET,
    Bucket: BUCKET_NAME,
});

var upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: BUCKET_NAME,
        acl: 'public-read',
        metadata: function (req, file, cb) {
            cb(null, {fieldName: file.fieldname});
        },
        key: function (req, file, cb) {
            // console.log(file);
            cb(null, Date.now().toString() + "_" + file.originalname);
        }
    })
})

router.get('/', auth.required, function(req, res, next){
    User.findById(req.payload.id).then(function(user){
        if(!user ) {
            return res.status(403).json({errors: {'user': 'does not exist'}});
        }

        var countQuery = (user.role === 'admin' ? {} : {owner: user.username});
        var findQuery = (user.role === 'admin'
                     ? {id: { $gt: req.query._start, $lte: req.query._end }}
                     : {id: { $gt: req.query._start, $lte: req.query._end }, owner: user.username});

        Video.count(countQuery).then(function(count) {
            var sortQuery = {};
            sortQuery[req.query._sort] = (req.query._order === 'DESC') ? -1 : 1;
            Video.find(countQuery).skip(parseInt(req.query._start)).limit(req.query._end - req.query._start).sort(sortQuery).then(function(items){
                return res.status(200)
                        .header({
                            'X-Total-Count': count,
                            'Access-Control-Expose-Headers': 'X-Total-Count',
                        })
                        .json(items.map(function(item, idx){
                            return item.toJSON();
                        }));
            }).catch(next);
        }).catch(next);
    }
)});

router.get('/:id', function(req, res, next){
    Video.findOne({id: req.params.id}).then(function(item){
        return item.toJSON();
    }).catch(next);
});

router.post('/', auth.required, function(req, res, next){
    User.findById(req.payload.id).then(function(user){
        if(!user ) {
            return res.status(403).json({errors: {'user': 'does not exist'}});
        }
        
        upload.any()(req, res, function(err){
                Video.count().then(function(count) {
                var video = new Video();
                video.id = count + 1;
                video.title = req.body.title;
                video.description = req.body.description;
                video.thumbnail = req.files[0].location;
                video.video = req.files[1].location;
                video.category = req.body.category;
                video.tag = req.body.tag;
                video.owner = user.username;
            
                video.save().then(function(){
                    return res.status(200).json(video.toJSON());
                }).catch(next);
            }).catch(next);
        });
    }).catch(next);    
});

router.put('/:id', auth.required, function(req, res, next){
    User.findById(req.payload.id).then(function(user){
        if(!user) {
            return res.status(403).json({errors: {'user': 'does not exist'}});
        }
            
        Video.findOneAndUpdate({id: req.params.id}, {
                title: req.body.title, 
                description: req.body.description,
                category: req.body.category,
                tag: req.body.tag,
            }).then(function(){
            return res.status(200).json({success: 'success'});
        }).catch(next);
    }).catch(next);
});

router.delete('/:id', auth.required, function(req, res, next){
    User.findById(req.payload.id).then(function(user){
        if(!user) {
            return res.status(403).json({errors: {'user': 'does not exist'}});
        }

        Video.findOne({id: req.params.id}).then(function(item){
            var params = {
                Bucket: BUCKET_NAME,
                Delete: {
                    Objects: [
                        { Key: item.thumbnail },
                        { Key: item.video },
                    ]
                },
            }

            s3.deleteObjects(params, function(err, data){
            })

            Video.remove({id: req.params.id}).then(function(){
                Video.find().then(function(items){
                    items.map(function(item, idx){
                        item.id = idx + 1;
                        item.save().then(function() {});
                    });
                    return res.status(200).json({success: 'success'});
                });
            }).catch(next);
        })
    }).catch(next);
});

module.exports = router;