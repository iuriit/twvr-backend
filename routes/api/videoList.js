var router = require('express').Router();
var mongoose = require('mongoose');
var Category = mongoose.model('Category');
var Video = mongoose.model('Video');

router.get('/', function(req, res, next){
    Category.find().then(function(categories){
        let array1 = [];
        categories.map((category, idx) => {
            Video.find({category: category.name}).then(function(videos){
                array1.push({
                    category: category.name,
                    video: videos.map(function(video){
                        return video.toJSON();
                    })
                });
                if(idx == categories.length - 1) {
                    return res.status(200).json({
                        categories: array1
                    });
                }
            })
        })
    })
});

router.get('/list', function(req, res, next){
    Video.find().then(function(items){
        return res.status(200)
                .json({video: items.map(function(item){
                    return item.toJSON();
                })});
    })
});

module.exports = router;