var router = require('express').Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Category = mongoose.model('Category');
var auth = require('../auth');

router.get('/', function(req, res, next){
    Category.count().then(function(count) {
        var sortQuery = {};
        sortQuery[req.query._sort] = (req.query._order === 'DESC') ? -1 : 1;
        Category.find({
            id: {
                $gt: req.query._start, 
                $lte: req.query._end,
            },
        }).sort(sortQuery).then(function(items){
            return res.status(200)
                    .header({
                        'X-Total-Count': count,
                        'Access-Control-Expose-Headers': 'X-Total-Count',
                    })
                    .json(items.map(function(item, idx){
                        return item.toJSON();
                    }));
        }).catch(next);
    }).catch(next);
});

router.get('/:id', function(req, res, next){
    Category.findOne({id: req.params.id}).then(function(item){
        return item.toJSON();
    }).catch(next);
});

router.post('/', auth.required, function(req, res, next){
    User.findById(req.payload.id).then(function(user){
        if(!user)
            return res.status(403).json({errors: {'user': 'does not exist'}});
        
        Category.count().then(function(count) {
            var category = new Category();
            category.id = count + 1;
            category.name = req.body.name;
            category.owner = user.username;
        
            category.save().then(function(){
                return res.status(200).json(category.toJSON());
            }).catch(next);
        }).catch(next);
    }).catch(next);    
});

router.put('/:id', auth.required, function(req, res, next){
    User.findById(req.payload.id).then(function(user){
        if(!user)
            return res.status(403).json({errors: {'user': 'does not exist'}});
        
        Category.findOneAndUpdate({id: req.params.id}, {name: req.body.name}).then(function(){
            return res.status(200).json({success: 'success'});
        }).catch(next);
    }).catch(next);    
});

router.delete('/:id', auth.required, function(req, res, next){
    User.findById(req.payload.id).then(function(user){
        if(!user)
            return res.status(403).json({errors: {'user': 'does not exist'}});
        
        Category.findOneAndRemove({id: req.params.id}).then(function(){
            Category.find().then(function(items){
                items.map(function(item, idx){
                    item.id = idx + 1;
                    item.save().then(function() {});
                });
                return res.status(200).json({success: 'success'});
            });
        }).catch(next);
    }).catch(next);    
});

module.exports = router;