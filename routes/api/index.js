var router = require('express').Router();

router.use('/', require('./login'));
router.use('/user', require('./users'));
router.use('/category', require('./category'));
router.use('/video', require('./video'));
router.use('/video_list', require('./videoList'));

router.use(function(err, req, res, next){
  if(err.name === 'ValidationError'){
    return res.json({
      errors: Object.keys(err.errors).reduce(function(errors, key){
        errors[key] = err.errors[key].message;

        return errors;
      }, {})
    });
  }

  return next(err);
});

module.exports = router;